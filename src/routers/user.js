const express = require('express');
const userRepository = require("../repositories/user");
const jwtHelper = require('../services/jwt')
const router = express.Router();


const createNewUser = async (req, res) => {
    try {
        const userData =  await userRepository.createUser(req.body);
        res.json({message: "createNewUser success operation",
                result: userData})
    } catch (e) {
        res.status(500).json({message: "createNewUser operation failed", result: e.message})
    }
};


const login = async (req, res) => {
    try {
        if ((req.body.password !== '12345')) {
            res.status(403).json({message:"unAuthorized"})
            return
        }
        const result = await userRepository.findUserByUsername(req.body.username)
        if (result) {
            const jsonWebToken = jwtHelper.jwtGenerator(req.body.username)
            res.json({message: ' login successfully', result: {jsonWebToken:jsonWebToken,username:req.query.username}})
        } else {
            res.status(404).json({message: 'unauthorized', result: result})
        }
    } catch (e) {
        res.status(500).json({message: "login operation failed", result: e.message})
    }
};


router.post('/register', createNewUser);
router.post('/login', login);
module.exports = router;

