const express = require('express');
const contactRepository = require("../repositories/contact");
const userRepository = require("../repositories/user");
const accessCheck = require('../middlewears/authorization')
const router = express.Router();


const addNewContact = async (req, res) => {
    try {
        const contactData = req.body
        contactData.username = res.locals.username
        const isPhoneDuplicatedForUser = await contactRepository.isPhoneDuplicatedForUser(contactData.username, contactData.phoneNumber, 0, 10)
        if (isPhoneDuplicatedForUser) {
            const result = await contactRepository.createContact(contactData);
            res.json({message: "contact successfully added", result: result})
        } else {
            res.status(400).json({message :"Duplicate phone number"})
        }
    } catch (e) {
        res.status(500).json({message: "add new contact operation failed", result: e.message})
    }
};


const updateContact = async (req, res) => {
    try {
        const contactData = req.body
        contactData.username = res.locals.username
        const result = await contactRepository.updateContact(contactData);
        res.json({message: "contact updated successfully", result: result})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};


const searchContactByDetails = async (req, res) => {
    try {
        let beginNum = parseInt(req.query.begin) || 0;
        let totalNum = parseInt(req.query.total) || 10;
        let keyWord = req.query.keyWord;
        const result = await contactRepository.searchContactByDetails(res.locals.username, keyWord, beginNum, totalNum);
        console.log("Result of search books ", result)
        res.json({"message": "your search result is: ", result: result})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};


const returnAllContact = async (req, res) => {
    try {
        let beginNum = parseInt(req.query.begin) || 0;
        let totalNum = parseInt(req.query.total) || 10;
        const result = await contactRepository.findAllContactsOfUser(res.locals.username,beginNum, totalNum);
        res.json({message: "your search result is: ", result: result})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}


const searchContactController = async (req, res) => {
    try {
        if (req.query.keyWord !== undefined) {
            const result = await searchContactByDetails(req, res);
            console.log("contact result:", result)
        } else {
            const result = await returnAllContact(req, res);
            console.log("contact result:", result)
        }
    } catch (e) {
        res.json({'message': e.message});
    }
};


router.post('/', accessCheck.validateJwt, addNewContact);
router.put('/', accessCheck.validateJwt, updateContact);
router.get('/', accessCheck.validateJwt, searchContactController);
module.exports = router;
