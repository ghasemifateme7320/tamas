const mongoose = require('mongoose');

let contactSchema = new mongoose.Schema({
    "phoneNumber": {type: String, index: true, require: true},
    "firstName": {type: String, index: true},
    "lastName": {type: String, index: true},
    "address": {type: String, index: true},
    "username":String
});


contactSchema.index({firstNAme: 1, lastName :1, phoneNumber :1, address :1});
contactSchema.index({
    firstName: 'text', lastName: 'text', phoneNumber: 'text',
    address: "text"
});


module.exports = {
    contactModel: mongoose.model('contact', contactSchema, 'contact'),
};
