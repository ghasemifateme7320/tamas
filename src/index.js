require('dotenv').config();
const express = require('express');
const app = express();
const db = require("./db/db-connection");
const bodyParser = require('body-parser');
app.use(bodyParser.json());



db.init();
console.log()


app.use('/contacts', require('./routers/contact'));
app.use('/users', require('./routers/user'));


app.listen(process.env.PORT, () => {
    console.log("Example app listening at http://%s:%s", process.env.PORT)
});
