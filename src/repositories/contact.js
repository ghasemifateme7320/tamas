const contactSchema = require('../db/models/contact').contactModel;


const createContact = async (data) => {
        const res = await contactSchema.create(
            {
                "username": data.username,
                "phoneNumber": data.phoneNumber,
                "firstName": data.firstName,
                "lastName": data.lastName,
                "address": data.address
            }
        )
    console.log("res: ",res)
    return res

};

const findAllContactsOfUser = (username, begin, total) => {
    return contactSchema.find({username: username}).sort({"lastName": -1}).skip(begin).limit(total)
}


const searchContactByDetails = (username, keyWord, begin, total) => {
    console.log('searchContactByDetails argumnets ', username, keyWord, begin, total)
    return contactSchema.find({username, $text: {$search: keyWord}}).skip(begin).limit(total)
};



const updateContact = (data) => {
    return contactSchema.findOneAndUpdate(
        {
            "phoneNumber": data.phoneNumber,
            "firstName": data.firstName,
            "lastName": data.lastName,
        },
        data
    )
};

const isPhoneDuplicatedForUser = async (username, phoneNumber, begin, total) => {
    const allUserContacts = await findAllContactsOfUser(username, begin, total)
    let notValidPhone = [];
    if(allUserContacts.length ===0){
        return true
    }
    else {
        for (let i = 0; i < allUserContacts.length; i++) {
            const contact = allUserContacts[i]
            if (contact.phoneNumber === phoneNumber) {
                notValidPhone.push(phoneNumber)
            }
        }
        return notValidPhone.length === 0;
    }
}




module.exports = {
    updateContact,
    searchContactByDetails,
    createContact,
    findAllContactsOfUser,
    isPhoneDuplicatedForUser
};
