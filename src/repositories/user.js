const userSchema = require('../db/models/user').userModel;

const createUser = async (data) => {
        return await userSchema.create(
            {
                username:data.username
            }
        )

};

const findUserByUsername = async (username)=>{
    return await userSchema.findOne({username: username})
}

module.exports = {createUser,findUserByUsername}
