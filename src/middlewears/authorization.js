const jwtHelper = require('../services/jwt');


const validateJwt = async (req, res, next) => {
    try {
        const authorizationHeader = jwtHelper.removeBearer(req.header('Authorization'));
        const userData = jwtHelper.verifyJwt(authorizationHeader)
        res.locals.username =userData
            // if(userData === req.body.username) {
                next()
        // } else {
        //     throw new Error("unAuthorize")
        // }
    } catch (e) {
        res.status(403).json({"message": "unAuthorize"})
    }
}



module.exports={validateJwt}
